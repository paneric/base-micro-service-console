# BASE MICRO SERVICE CONSOLE

> Make sure the `scope` variable is set as `lib`.

*bin/app*
```php
#!/usr/bin/env php
<?php

$scope = 'lib';
...
```
Run console command:

```sh
$ bin/app bms

Psr-4 (no trailing backslash !!!): ECommerce
Vendor service folder path (relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-session
Service name (CamelCase !!!): Session
DB table column name prefix (lower case !!!): ses
DB name: e_commerce
DAO attributes names (camelCase, coma separated !!!): sessionId,ipAddress,userAgent,data
DAO attributes types (CamelCase, coma separated !!!): String,String,String,String
DAO unique attribute name (camelCase !!!): sessionId

                                                                          
  BASE MICRO SERVICE UPDATE SUCCESS:                                      
                                                                          

  Resources update with vendor "ECommerce", service "Session" and prefix "ses" success. 


```
