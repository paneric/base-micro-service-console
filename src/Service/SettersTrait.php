<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Service;

trait SettersTrait
{
    public function set(string $value): string
    {
        return $value;
    }

    public function set_UCF(string $service): string
    {
        return ucfirst($service);
    }

    protected function set_lcf(string $attribute): string
    {
        return lcfirst($attribute);
    }

    public function set_lc_sc(string $value): string
    {
        return strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/','_$1', $value));
    }

    public function setComaSubPrefixes($subPrefixes): string
    {
        if (!is_array($subPrefixes)) {
            return $subPrefixes;
        }

        foreach ($subPrefixes as $index => $subPrefix) {
            $subPrefixes[$index] = " '" . $subPrefix . "s'";
        }

        return implode(',', $subPrefixes);
    }

    public function setApiPrefix(string $prefix): string
    {
        return 'api-' . $prefix;
    }

    public function setApcPrefix(string $prefix): string
    {
        return 'apc-' . $prefix;
    }
}
