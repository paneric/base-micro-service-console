<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Service\Statement;

use Paneric\BMSConsole\Service\Statement\Preparator\DAOPreparator;
use Paneric\BMSConsole\Service\Statement\Preparator\SettingsValidationPreparator;
use Paneric\BMSConsole\Service\Statement\Preparator\TemplatesAddEditPreparator;
use Paneric\BMSConsole\Service\Statement\Preparator\TemplatesAddEditRemoveMultiplePreparator;
use Paneric\BMSConsole\Service\Statement\Preparator\TemplatesShowAllPreparator;

class StatementsPreparator
{
    protected $daoPreparator;
    protected $settingsValidationPreparator;
    protected $templatesAddEditPreparator;
    protected $templatesAddEditRemoveMultiplePreparator;
    protected $templatesShowAllPreparator;

    protected $settings;

    public function __construct(
        DAOPreparator $daoPreparator,
        SettingsValidationPreparator $settingsValidationPreparator,
        TemplatesAddEditPreparator $templatesAddEditPreparator,
        TemplatesAddEditRemoveMultiplePreparator $templatesAddEditRemoveMultiplePreparator,
        TemplatesShowAllPreparator $templatesShowAllPreparator,
        array $settings
    ) {
        $this->daoPreparator = $daoPreparator;
        $this->settingsValidationPreparator = $settingsValidationPreparator;
        $this->templatesAddEditPreparator = $templatesAddEditPreparator;
        $this->templatesAddEditRemoveMultiplePreparator = $templatesAddEditRemoveMultiplePreparator;
        $this->templatesShowAllPreparator = $templatesShowAllPreparator;

        $this->settings = $settings;
    }

    public function prepare(
        string $filePath,
        array $attributes,
        array $attributesTypes
    ): array {
        $statements = [];

        if (isset($this->settings['files'][$filePath])) {
            $converter = $this->settings['files'][$filePath];

            if ($converter === 'dao') {
                return $this->daoPreparator->prepare(
                    $this->settings['dao'],
                    $attributes,
                    $attributesTypes
                );
            }

            if ($converter === 'settings_validation') {
                return $this->settingsValidationPreparator->prepare(
                    $this->settings['settings_validation'],
                    $attributes
                );
            }

            if ($converter === 'templates_add') {
                return $this->templatesAddEditPreparator->prepare(
                    $this->settings['templates_add'],
                    $attributes
                );
            }

            if ($converter === 'templates_edit') {
                return $this->templatesAddEditPreparator->prepare(
                    $this->settings['templates_edit'],
                    $attributes
                );
            }

            if ($converter === 'templates_show_all' ) {
                return $this->templatesShowAllPreparator->prepare(
                    $this->settings['templates_show_all'],
                    $attributes
                );
            }

            if ($converter === 'templates_add_multiple') {
                return $this->templatesAddEditRemoveMultiplePreparator->prepare(
                    $this->settings['templates_add_multiple'],
                    $attributes
                );
            }

            if ($converter === 'templates_edit_multiple') {
                return $this->templatesAddEditRemoveMultiplePreparator->prepare(
                    $this->settings['templates_edit_multiple'],
                    $attributes
                );
            }
        }

        return $statements;
    }
}
