<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Service\Statement\Preparator;

class TemplatesAddEditPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $attributes): array
    {
        $stringifiedStatements['attributes'] = $this->prepareWithAttributes(
            $statements['attributes'],
            $attributes
        );

        return $stringifiedStatements;
    }
}
