<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Service\Statement\Preparator;

class TemplatesAddEditRemoveMultiplePreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $attributes): array
    {
        $stringifiedStatements['title_attribute'] = $this->prepareWithAttributes(
            $statements['title_attribute'],
            $attributes
        );

        $stringifiedStatements['attributes'] = $this->prepareWithAttributes(
            $statements['attributes'],
            $attributes
        );

        return $stringifiedStatements;
    }
}
