<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Service\Statement\Preparator;

use Paneric\BMSConsole\Service\ParamsTrait;
use Paneric\BMSConsole\Service\SettersTrait;

trait PreparatorsTrait
{
    use ParamsTrait;
    use SettersTrait;

    protected function prepareWithSubServicesSubPrefixes(
        array $statement,
        array $SubServices,
        array $subprefixes
    ): string {
        $stringifiedStatement = '';

        foreach ($SubServices as $index => $SubService) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'SubService' => $SubService,
                'subprefix' => $subprefixes[$index]
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWithAttributes(array $statement, array $attributes): string
    {
        $stringifiedStatement = '';

        foreach ($attributes as $attribute) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'attribute' => $attribute
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWithAttributesTypes(array $statement, array $attributes, array $Types): string
    {
        $stringifiedStatement = '';

        foreach ($attributes as $index => $attribute) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'attribute' => $attribute,
                'Type' => $Types[$index]
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWithSubServices(array $statement, array $SubServices): string
    {
        $stringifiedStatement = '';

        foreach ($SubServices as $SubService) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'SubService' => $SubService
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }

    protected function prepareWithSubPrefixes(array $statement, array $subprefixes): string
    {
        $stringifiedStatement = '';

        foreach ($subprefixes as $subprefix) {

            $extractor = [
                'methods' => $statement['methods'],
                'values' => $statement['values'],
                'subprefix' => $subprefix
            ];

            $stringifiedStatement .= preg_replace(
                $statement['patterns'],
                $this->setParams($extractor),
                $statement['template']
            );
        }

        return $stringifiedStatement;
    }
}
