<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Service\Statement\Preparator;

class DAOPreparator
{
    use PreparatorsTrait;

    public function prepare(array $statements, array $attributes, array $Types): array
    {
        $stringifiedStatements = [];

        foreach ($statements as $key => $statement) {
            $stringifiedStatements[$key] = $this->prepareWithAttributesTypes(
                $statement,
                $attributes,
                $Types
            );
        }

        return $stringifiedStatements;
    }
}
