<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Service;

class PatternsConverter
{
    use ParamsTrait;
    use SettersTrait;

    protected $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function convert(
        string $stringifiedFile,
        string $psr4,
        string $vendorServicePath,
        string $service,
        string $prefix,
        string $uniqueAttribute
    ): string {
        $extractor = [
            'methods' => $this->settings['methods'],
            'values' => $this->settings['values'],
            'Psr4' => $psr4,
            'vendorServicePath' => $vendorServicePath,
            'service' => $service,
            'prefix' => $prefix,
            'uniqueAttribute' => $uniqueAttribute
        ];

        return preg_replace(
            $this->settings['patterns'],
            $this->setParams($extractor),
            $stringifiedFile
        );
    }
}
