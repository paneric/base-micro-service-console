<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Service;

use Paneric\BMSConsole\Service\Statement\StatementsConverter;
use Paneric\BMSConsole\Service\Statement\StatementsPreparator;
use RuntimeException;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

class BMSService
{
    use SettersTrait;

    protected $statementsPreparator;
    protected $patternsConverter;
    protected $statementsConverter;
    protected $filesScanner;
    protected $settings;
    protected $resourcesDir;
    protected $outputDir;

    public function __construct(
        StatementsPreparator $statementsPreparator,
        PatternsConverter $patternsConverter,
        StatementsConverter $statementsConverter,
        FilesScanner $filesScanner,
        array $settings,
        string $resourcesDir,
        string $outputDir = null
    ) {
        $this->statementsPreparator = $statementsPreparator;
        $this->patternsConverter = $patternsConverter;
        $this->statementsConverter = $statementsConverter;
        $this->filesScanner = $filesScanner;
        $this->settings = $settings;
        $this->resourcesDir = $resourcesDir;
        $this->outputDir = $outputDir;
    }

    public function  convert(
        OutputInterface $output,
        string $psr4,
        string $vendorServicePath,
        string $service,
        string $prefix,
        array $attributes,
        array $attributesTypes,
        string $uniqueAttribute
    ): void {

        $files = [];
        $files = $this->filesScanner->scanDirectory($this->resourcesDir, $files);

        foreach ($files as $filePath) {
            $stringifiedFile = file_get_contents ($filePath);

            $statements = $this->statementsPreparator->prepare(
                $filePath,
                $attributes,
                $attributesTypes
            );

            if (!empty($statements)) {
                $stringifiedFile = $this->statementsConverter->convert($stringifiedFile, $statements);
            }

            $stringifiedFile = $this->patternsConverter->convert(
                $stringifiedFile,
                $psr4,
                $vendorServicePath,
                $service,
                $prefix,
                $uniqueAttribute
            );

            $filePathOutput = $this->setFilePathOutput($service, $filePath);

            $this->createDirectory($filePathOutput);

            $result = $this->createFile($output, $psr4, $service, $prefix, $filePathOutput, $stringifiedFile);

            if (!$result) {
                return;
            }
        }

        $this->setOutput($output, $psr4, $service, $prefix);
    }

    protected function setFilePathOutput(string $service, string $filePath): string
    {
        $serviceCCname = $this->set($service);

        return str_replace(
            [$this->resourcesDir, 'BaseService'],
            [$this->outputDir, $serviceCCname],
            $filePath
        );
    }

    protected function createDirectory(string $filePathOutput): void
    {
        $filePathOutputExploded = explode(DIRECTORY_SEPARATOR, $filePathOutput);

        array_pop($filePathOutputExploded);

        $dirPath = implode(DIRECTORY_SEPARATOR, $filePathOutputExploded);

        if (!file_exists($dirPath)) {
            if (!mkdir($dirPath, 0777, true) && !is_dir($dirPath)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $dirPath));
            }
        }
    }

    protected function createFile(
        OutputInterface $output,
        string $psr4,
        string $service,
        string $prefix,
        string $filePathOutput,
        string $stringifiedFile
    ): bool {
        $fileHandle = fopen($filePathOutput, 'wb');

        if ($fileHandle === false) {
            $this->setErrorOutput($output, $psr4, $service, $prefix);
            return false;
        }

        if (fwrite($fileHandle, $stringifiedFile) === false) {
            fclose($fileHandle);

            $this->setErrorOutput($output, $psr4, $service, $prefix);
            return false;
        }

        fclose($fileHandle);

        return true;
    }

    protected function setErrorOutput(
        OutputInterface $output,
        string $psr4,
        string $service,
        string $prefix
    ): void {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('white', 'red', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  BASE MICRO SERVICE UPDATE FAILURE:                                      </>',
            '<title>                                                                          </>',
            '',
            $this->setErrorMessage($psr4, $service, $prefix),
            ''
        ]);
    }

    protected function setErrorMessage(string $psr4, string $service, string $prefix): string
    {
        return sprintf(
            '<options=bold>  Resources update with vendor "%s", service "%s" and prefix "%s" failure. </>',
            $psr4,
            $service,
            $prefix,
        );
    }

    protected function setOutput(
        OutputInterface $output,
        string $psr4,
        string $service,
        string $prefix
    ): void {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('white', 'green', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  BASE MICRO SERVICE UPDATE SUCCESS:                                      </>',
            '<title>                                                                          </>',
            '',
            sprintf(
                '<options=bold>  Resources update with vendor "%s", service "%s" and prefix "%s" success. </>',
                $psr4,
                $service,
                $prefix,
            ),
            ''
        ]);
    }
}
