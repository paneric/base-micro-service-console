<?php

return [

    'title_attribute' => [
        'template' =>
<<<END
                    <th class="text-center">{{ '{attribute}'|trans }}</th>

END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],

    'attributes' => [
        'template' =>
<<<END
                            <td class="text-center">
                                <div class="form-group  {{ vld_style('{attribute}', i) }}">
                                    <input class="form-group-item form-group-input" type="text" name="{attribute}[{{ i }}]" value="{{ {prefix}.{attribute} }}" placeholder="{{ loop.index }}" autocomplete="off" required/>
                                    <label for="name" class="form-group-item form-group-label"><span></span></label>
                                </div>
                                {{ vld_feedback('{attribute}', i)|raw }}
                            </td>

END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],
];
