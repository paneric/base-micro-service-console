<?php

return [

    'attributes' => [
        'template' =>
<<<END
            <div class="form-group  {{ vld_style('{attribute}') }}">
                <input class="form-group-item form-group-input" type="text" name="{attribute}" value="{{ {prefix}.{attribute} }}" autocomplete="off" required/>
                <label for="name" class="form-group-item form-group-label"><span>{{ '{attribute}'|trans }}</span></label>
            </div>
            {{ vld_feedback('{attribute}')|raw }}


END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],

];
