<?php

return [
    'attributes' => [
        'template' =>
<<<END
                    '{attribute}' => [
                        'required' => [],
                    ], \n
END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],

];
