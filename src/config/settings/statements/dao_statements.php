<?php

return [

    'attributes' => [
            'template' =>
<<<END
    protected \${attribute};//{Type} \n
END,
        'patterns' => [ '/{attribute}/',  '/{Type}/'],
        'methods'  => [           'set',       'set'],
        'values'   => [     'attribute',      'Type'],
    ],


    'getters' => [
        'template' =>
<<<END
    public function get{Attribute}(): ?{Type}
    {
        return \$this->{attribute};
    } \n
END,
        'patterns' => [ '/{attribute}/',  '/{Attribute}/',  '/{Type}/'],
        'methods'  => [           'set',        'set_UCF',       'set'],
        'values'   => [     'attribute',      'attribute',      'Type'],
    ],


    'setters' => [
        'template' =>
<<<END
    public function set{Attribute}({Type} \${attribute}): void
    {
        \$this->{attribute} = \${attribute};
    } \n
END,
        'patterns' => [ '/{attribute}/',  '/{Attribute}/',  '/{Type}/'],
        'methods'  => [           'set',        'set_UCF',   'set_UCF'],
        'values'   => [     'attribute',      'attribute',      'Type'],
    ],

];
