<?php

return [

    'title_attribute' => [
        'template' =>
<<<END
                    <th class="text-center">{{ '{attribute}'|trans }}</th>

END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],

    'attributes' => [
        'template' =>
<<<END
                        <td class="text-center">{{ {prefix}.{attribute} }}</td>

END,
        'patterns' => ['/{attribute}/'],
        'methods'  => [    'set_lc_sc'],
        'values'   => [    'attribute'],
    ],
];
