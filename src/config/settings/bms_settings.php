<?php

return [
    'bms' => [
        'files' => [
            APP_FOLDER . 'resources_bms/Gateway/BaseServiceDAO.php' => 'dao',

            APP_FOLDER . 'resources_bms/BaseServiceApi/config/settings/validation.php' => 'settings_validation',
            APP_FOLDER . 'resources_bms/BaseServiceApp/config/settings/validation.php' => 'settings_validation',

            APP_FOLDER . 'resources_bms/BaseServiceApc/templates/add.html.twig'                => 'templates_add',
            APP_FOLDER . 'resources_bms/BaseServiceApp/templates/add.html.twig'                => 'templates_add',
            APP_FOLDER . 'resources_bms/BaseServiceApc/templates/edit.html.twig'               => 'templates_edit',
            APP_FOLDER . 'resources_bms/BaseServiceApp/templates/edit.html.twig'               => 'templates_edit',

            APP_FOLDER . 'resources_bms/BaseServiceApc/templates/add_multiple.html.twig'       => 'templates_add_multiple',
            APP_FOLDER . 'resources_bms/BaseServiceApp/templates/add_multiple.html.twig'       => 'templates_add_multiple',
            APP_FOLDER . 'resources_bms/BaseServiceApc/templates/edit_multiple.html.twig'      => 'templates_edit_multiple',
            APP_FOLDER . 'resources_bms/BaseServiceApp/templates/edit_multiple.html.twig'      => 'templates_edit_multiple',

            APP_FOLDER . 'resources_bms/BaseServiceApc/templates/remove_multiple.html.twig'    => 'templates_show_all',
            APP_FOLDER . 'resources_bms/BaseServiceApp/templates/remove_multiple.html.twig'    => 'templates_show_all',
            APP_FOLDER . 'resources_bms/BaseServiceApc/templates/show_one_by_id.html.twig'     => 'templates_show_all',
            APP_FOLDER . 'resources_bms/BaseServiceApp/templates/show_one_by_id.html.twig'     => 'templates_show_all',
            APP_FOLDER . 'resources_bms/BaseServiceApc/templates/show_all.html.twig'           => 'templates_show_all',
            APP_FOLDER . 'resources_bms/BaseServiceApp/templates/show_all.html.twig'           => 'templates_show_all',
            APP_FOLDER . 'resources_bms/BaseServiceApc/templates/show_all_paginated.html.twig' => 'templates_show_all',
            APP_FOLDER . 'resources_bms/BaseServiceApp/templates/show_all_paginated.html.twig' => 'templates_show_all',
        ],

        'params' => [
            'patterns' => ['/{Psr4}/', '/{vendorServicePath}/', '/{base_service}/', '/{BaseService}/', '/{prefix}/', '/{api-prefix}/', '/{apc-prefix}/', '/{unique_attribute}/'],
            'methods'  => [     'set',                   'set',        'set_lc_sc',             'set',        'set',   'setApiPrefix',   'setApcPrefix',            'set_lc_sc'],
            'values'   => [    'Psr4',     'vendorServicePath',          'service',         'service',     'prefix',         'prefix',         'prefix',      'uniqueAttribute'],
        ],

        'dao' => require 'statements/dao_statements.php',

        'settings_validation' => require 'statements/settings_validation_statements.php',

        'templates_add'           => require 'statements/templates_add_statements.php',
        'templates_edit'          => require 'statements/templates_edit_statements.php',

        'templates_add_multiple'  => require 'statements/templates_add_multiple_statements.php',
        'templates_edit_multiple' => require 'statements/templates_edit_multiple_statements.php',

        'templates_show_all' => require 'statements/templates_show_all_statements.php',
    ],
];
