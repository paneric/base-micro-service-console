<?php

declare(strict_types=1);

use Paneric\BMSConsole\Service\FilesScanner;
use Paneric\BMSConsole\Service\BMSService;
use Paneric\BMSConsole\Service\PatternsConverter;
use Paneric\BMSConsole\Service\Statement\Preparator\DAOPreparator;
use Paneric\BMSConsole\Service\Statement\Preparator\SettingsValidationPreparator;
use Paneric\BMSConsole\Service\Statement\Preparator\TemplatesAddEditPreparator;
use Paneric\BMSConsole\Service\Statement\Preparator\TemplatesAddEditRemoveMultiplePreparator;
use Paneric\BMSConsole\Service\Statement\Preparator\TemplatesShowAllPreparator;
use Paneric\BMSConsole\Service\Statement\StatementsConverter;
use Paneric\BMSConsole\Service\Statement\StatementsPreparator;
use Psr\Container\ContainerInterface;

return [
    BMSService::class => static function (ContainerInterface $container): BMSService
    {
        return new BMSService(
            $container->get(StatementsPreparator::class),
            $container->get(PatternsConverter::class),
            $container->get(StatementsConverter::class),
            $container->get(FilesScanner::class),
            $container->get('bms'),
            $container->get('bms_resources_dir'),
            $container->get('bms_output_dir')
        );
    },

    FilesScanner::class => static function (): FilesScanner
    {
        return new FilesScanner();
    },

    PatternsConverter::class => static function (ContainerInterface $container): PatternsConverter
    {
        return new PatternsConverter(
            $container->get('bms')['params'],
        );
    },

    StatementsPreparator::class => static function (ContainerInterface $container): StatementsPreparator
    {
        return new StatementsPreparator(
            $container->get(DAOPreparator::class),
            $container->get(SettingsValidationPreparator::class),
            $container->get(TemplatesAddEditPreparator::class),
            $container->get(TemplatesAddEditRemoveMultiplePreparator::class),
            $container->get(TemplatesShowAllPreparator::class),
            $container->get('bms')
        );
    },

    DAOPreparator::class => static function (): DAOPreparator
    {
        return new DAOPreparator();
    },
    SettingsValidationPreparator::class => static function (): SettingsValidationPreparator
    {
        return new SettingsValidationPreparator();
    },
    TemplatesAddEditPreparator::class => static function (): TemplatesAddEditPreparator
    {
        return new TemplatesAddEditPreparator();
    },
    TemplatesAddEditRemoveMultiplePreparator::class => static function (): TemplatesAddEditRemoveMultiplePreparator
    {
        return new TemplatesAddEditRemoveMultiplePreparator();
    },
    TemplatesShowAllPreparator::class => static function (): TemplatesShowAllPreparator
    {
        return new TemplatesShowAllPreparator();
    },


    StatementsConverter::class => static function (): StatementsConverter
    {
        return new StatementsConverter();
    },
];
