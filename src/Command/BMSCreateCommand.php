<?php

declare(strict_types=1);

namespace Paneric\BMSConsole\Command;

use Paneric\BMSConsole\Service\BMSService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Psr\Container\ContainerInterface;

class BMSCreateCommand extends Command
{
    protected static $defaultName = 'bms';

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Creates interfaces.')
            ->setHelp('This command allows you to create interfaces.')
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_REQUIRED,
                'Choice of required class.',
                'bms'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        $questionHelper = $this->getHelper('question');
//        $psr4 = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Psr-4 (no trailing backslash !!!): ')
//        );
//        $vendorServicePath = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Vendor service folder path (relative to project folder, no trailing slash !!!): ')
//        );
//
//
//        $service = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('Service name (CamelCase !!!): ')
//        );
//        $prefix = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('DB table column name prefix (lower case !!!): ')
//        );
//
//
//        $attributes = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('DAO attributes names (camelCase, coma separated !!!): ')
//        );
//        $attributesTypes = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('DAO attributes types (CamelCase, coma separated !!!): ')
//        );
//        $uniqueAttribute = $questionHelper->ask(
//            $input,
//            $output,
//            new Question('DAO unique attribute name (camelCase !!!): ')
//        );


        $adapter = $this->container->get(BMSService::class);

        if ($adapter === null) {
            $this->setModeErrorOutput($output);

            return 0;
        }

//        $adapter->convert(
//            $output,
//            $psr4,
//            $vendorServicePath,
//            $service,
//            $prefix,
//            explode(',', str_replace(' ','', $attributes)),
//            explode(',', str_replace(' ','', $attributesTypes)),
//            $uniqueAttribute
//        );

        $adapter->convert(
            $output,
            'ECommerce',
            'vendor/paneric/e-commerce-address',
            'Address',
            'adr',
            explode(',', str_replace(' ','', 'ref')),
            explode(',', str_replace(' ','', 'string')),
            'ref'
        );

        return 0;
    }

    protected function setModeErrorOutput(OutputInterface $output): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('white', 'red', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  ERROR:                                                                  </>',
            '<title>                                                                          </>',
            '',
            $this->setModeErrorMessage(),
            ''
        ]);
    }

    protected function setModeErrorMessage(): string
    {
        return '<fg=red;options=bold> Something\'s wrong, check logs. </>';
    }
}

