<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\{BaseService}Api\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleApiController;

class {BaseService}ApiController extends BaseModuleApiController {}
