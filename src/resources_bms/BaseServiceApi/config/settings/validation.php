<?php

declare(strict_types=1);

use {Psr4}\{BaseService}\Gateway\{BaseService}DTO;

return [

    'validation' => [

        'api-{prefix}.create' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
{attributes}
                ],
            ],
        ],

        'api-{prefix}s.create' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
{attributes}
                ],
            ],
        ],

        'api-{prefix}.update' => [
            'methods' => ['PUT'],
            {BaseService}DTO::class => [
                'rules' => [
{attributes}
                ],
            ],
        ],

        'api-{prefix}s.update' => [
            'methods' => ['PUT'],
            {BaseService}DTO::class => [
                'rules' => [
{attributes}
                ],
            ],
        ],

        'api-{prefix}s.delete' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ]
                ],
            ],
        ],
    ]
];
