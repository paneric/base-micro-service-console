<?php

declare(strict_types=1);

use {Psr4}\{BaseService}\config\{BaseService}RepositoryConfig;
use {Psr4}\{BaseService}\Repository\{BaseService}Repository;
use {Psr4}\{BaseService}\Repository\{BaseService}RepositoryInterface;
use Paneric\DBAL\Manager;
use Psr\Container\ContainerInterface;

return [
    {BaseService}RepositoryInterface::class => static function (ContainerInterface $container): {BaseService}Repository {
        return new {BaseService}Repository(
            $container->get(Manager::class),
            $container->get({BaseService}RepositoryConfig::class)
        );
    },
];
