<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\Gateway;

use Paneric\DataObject\DAO;

class {BaseService}DAO extends DAO
{
    protected $id;
    {attributes}
    public function __construct()
{
    $this->prefix = '{prefix}_';

    $this->setMaps();
}

    /**
     * @return null|int|string
     */
    public function getId()
{
    return $this->id;
}
    {getters}
    /**
     * @var int|string
     */
    public function setId($id): void
{
    $this->id = $id;
}
    {setters}
}
