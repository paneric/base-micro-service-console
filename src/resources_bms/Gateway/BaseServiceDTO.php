<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\Gateway;

class {BaseService}DTO extends {BaseService}DAO {}
