<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\Repository;

use Paneric\ComponentModule\Interfaces\Repository\ModuleRepositoryInterface;

interface {BaseService}RepositoryInterface extends ModuleRepositoryInterface
{}
