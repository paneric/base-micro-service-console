<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\Repository;

use Paneric\ComponentModule\Module\Repository\ModuleRepository;

class {BaseService}Repository extends ModuleRepository implements {BaseService}RepositoryInterface {}
