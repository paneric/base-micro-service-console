<?php

declare(strict_types=1);

return [
    'default_route_key' => '{prefix}',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => false,
    'module_map' => [
        '{prefix}'      => ROOT_FOLDER . 'src/{BaseService}App',
        '{prefix}s'     => ROOT_FOLDER . 'src/{BaseService}App',
        'api-{prefix}'  => ROOT_FOLDER . 'src/{BaseService}Api',
        'api-{prefix}s' => ROOT_FOLDER . 'src/{BaseService}Api',
        'apc-{prefix}'  => ROOT_FOLDER . 'src/{BaseService}Apc',
        'apc-{prefix}s' => ROOT_FOLDER . 'src/{BaseService}Apc',
    ],
    'module_map_cross' => [
        '{prefix}'       => ROOT_FOLDER . '{vendorServicePath}/src/{BaseService}App',
        '{prefix}s'      => ROOT_FOLDER . '{vendorServicePath}/src/{BaseService}App',
        'api-{prefix}'   => ROOT_FOLDER . '{vendorServicePath}/src/{BaseService}Api',
        'api-{prefix}s'  => ROOT_FOLDER . '{vendorServicePath}/src/{BaseService}Api',
        'apc-{prefix}'   => ROOT_FOLDER . '{vendorServicePath}/src/{BaseService}Apc',
        'apc-{prefix}s'  => ROOT_FOLDER . '{vendorServicePath}/src/{BaseService}Apc',
    ],
];
