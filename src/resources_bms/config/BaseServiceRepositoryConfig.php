<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\config;

use {Psr4}\{BaseService}\Gateway\{BaseService}DAO;
use Paneric\Interfaces\Config\ConfigInterface;
use PDO;

class {BaseService}RepositoryConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'table' => '{base_service}s',
            'dao_class' => {BaseService}DAO::class,
            'fetch_mode' => PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
            'create_unique_where' => sprintf(
                ' %s',
                'WHERE {prefix}_ref=:{prefix}_ref'
            ),
            'update_unique_where' => sprintf(
                ' %s %s',
                'WHERE {prefix}_ref=:{prefix}_ref',
                'AND {prefix}_id NOT IN (:{prefix}_id)'
            ),
        ];
    }
}
