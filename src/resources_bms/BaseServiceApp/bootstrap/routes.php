<?php

declare(strict_types=1);

use {Psr4}\{BaseService}\{BaseService}App\Controller\{BaseService}AppController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->map(['GET'], '/{prefix}/show-one-by-id/{id}', function(Request $request, Response $response, array $args) {
        return $this->get({BaseService}AppController::class)->showOneById(
            $response,
            $this->get('{base_service}_get_one_by_id_app_action'),
            $args['id'] ?? '1'
        );
    })->setName('{prefix}.show-one-by-id');


    $app->map(['GET'], '/{prefix}s/show-all', function(Request $request, Response $response) {
        return $this->get({BaseService}AppController::class)->showAll(
            $response,
            $this->get('{base_service}_get_all_app_action')
        );
    })->setName('{prefix}s.show-all');

    $app->map(['GET'], '/{prefix}s/show-all-paginated[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get({BaseService}AppController::class)->showAllPaginated(
            $request,
            $response,
            $this->get('{base_service}_get_all_paginated_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('{prefix}s.show-all-paginated')
        ->addMiddleware($container->get(PaginationMiddleware::class));


    $app->map(['GET', 'POST'], '/{prefix}/add', function(Request $request, Response $response) {
        return $this->get({BaseService}AppController::class)->add(
            $request,
            $response,
            $this->get('{base_service}_create_app_action')
        );
    })->setName('{prefix}.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/{prefix}s/add', function(Request $request, Response $response) {
        return $this->get({BaseService}AppController::class)->addMultiple(
            $request,
            $response,
            $this->get('{base_service}_create_multiple_app_action')
        );
    })->setName('{prefix}s.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));


    $app->map(['GET', 'POST'], '/{prefix}/edit/{id}', function(Request $request, Response $response, array $args) {
        return $this->get({BaseService}AppController::class)->edit(
            $request,
            $response,
            $this->get('{base_service}_get_one_by_id_app_action'),
            $this->get('{base_service}_update_app_action'),
            $args['id']
        );
    })->setName('{prefix}.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/{prefix}s/edit[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get({BaseService}AppController::class)->editMultiple(
            $request,
            $response,
            $this->get('{base_service}_get_all_paginated_app_action'),
            $this->get('{base_service}_update_multiple_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('{prefix}s.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(PaginationMiddleware::class));


    $app->map(['GET', 'POST'], '/{prefix}/remove/{id}', function(Request $request, Response $response, array $args) {
        return $this->get({BaseService}AppController::class)->remove(
            $request,
            $response,
            $this->get('{base_service}_delete_app_action'),
            $args['id']
        );
    })->setName('{prefix}.remove')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/{prefix}s/remove[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get({BaseService}AppController::class)->removeMultiple(
            $request,
            $response,
            $this->get('{base_service}_get_all_paginated_app_action'),
            $this->get('{base_service}_delete_multiple_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('{prefix}s.remove')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(PaginationMiddleware::class));
}
