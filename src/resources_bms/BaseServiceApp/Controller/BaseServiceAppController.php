<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\{BaseService}App\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleAppController;

class {BaseService}AppController extends BaseModuleAppController {}
