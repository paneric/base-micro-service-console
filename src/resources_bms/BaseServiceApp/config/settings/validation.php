<?php

declare(strict_types=1);

use {Psr4}\{BaseService}\Gateway\{BaseService}DTO;

return [

    'validation' => [

        '{prefix}.add' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
{attributes}
                ],
            ],
        ],

        '{prefix}s.add' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
{attributes}
                ],
            ],
        ],

        '{prefix}.edit' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
{attributes}
                ],
            ],
        ],

        '{prefix}s.edit' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
{attributes}
                ],
            ],
        ],

        '{prefix}.remove' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],

        '{prefix}s.remove' => [
            'methods' => ['POST'],
            {BaseService}DTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],
    ]
];
