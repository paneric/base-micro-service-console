<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\{BaseService}App\config;

use {Psr4}\{BaseService}\Gateway\{BaseService}DAO;
use {Psr4}\{BaseService}\Gateway\{BaseService}DTO;
use Paneric\Interfaces\Config\ConfigInterface;

class {BaseService}AppActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'get_one_by_id' => [
                'module_name_sc' => '{base_service}',
                'prefix' => '{prefix}',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['{prefix}_id' => (int) $id];
                },
            ],

            'get_all' => [
                'module_name_sc' => '{base_service}',
                'prefix' => '{prefix}',
                'order_by' => static function (string $local = null): array
                {
                    return [];
                },
            ],

            'get_all_paginated' => [
                'module_name_sc' => '{base_service}',
                'prefix' => '{prefix}',
                'find_by_criteria' => static function (string $local = null): array
                {
                    return [];
                },
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ],

            'create' => [
                'module_name_sc' => '{base_service}',
                'prefix' => '{prefix}',
                'dao_class' => {BaseService}DAO::class,
                'dto_class' => {BaseService}DTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['{prefix}_ref' => $attributes['ref']];
                },
            ],

            'create_multiple' => [
                'module_name_sc' => '{base_service}',
                'prefix' => '{prefix}',
                'dao_class' => {BaseService}DAO::class,
                'dto_class' => {BaseService}DTO::class,
                'create_unique_criteria' => static function (array $collection): array
                {
                    $createUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $createUniqueCriteria[$index] = ['{prefix}_ref' => $dao->getRef()];
                    }

                    return $createUniqueCriteria;
                },
            ],

            'update' => [
                'module_name_sc' => '{base_service}',
                'prefix' => '{prefix}',
                'dao_class' => {BaseService}DAO::class,
                'dto_class' => {BaseService}DTO::class,
                'find_one_by_criteria' => static function ({BaseService}DAO $dao, string $id): array
                {
                    return ['{prefix}_id' => (int) $id, '{prefix}_ref' => $dao->getRef()];
                },
                'update_unique_criteria' => static function (string $id): array
                {
                    return ['{prefix}_id' => (int) $id];
                },
            ],

            'update_multiple' => [
                'module_name_sc' => '{base_service}',
                'prefix' => '{prefix}',
                'dao_class' => {BaseService}DAO::class,
                'dto_class' => {BaseService}DTO::class,
                'find_by_criteria' => static function (array $collection): array
                {
                    $findByCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $findByCriteria[$index] = [
                            '{prefix}_id' => (int) $index,
                            '{prefix}_ref' => $dao->getRef(),
                        ];
                    }

                    return $findByCriteria;
                },

                'update_unique_criteria' => static function (array $collection): array
                {
                    $updateUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $updateUniqueCriteria[$index] = [
                            '{prefix}_id' => (int) $index,
                        ];
                    }

                    return $updateUniqueCriteria;
                },
            ],

            'delete' => [
                'module_name_sc' => '{base_service}',
                'delete_by_criteria' => static function (array $attributes): array
                {
                    $deleteByCriteria = [];

                    foreach ($attributes as $key => $value) {
                        $deleteByCriteria['{prefix}_' . $key] = (int) $value;
                    }

                    return $deleteByCriteria;
                },
            ],

            'delete_multiple' => [
                'module_name_sc' => '{base_service}',
                'dao_class' => {BaseService}DAO::class,
                'dto_class' => {BaseService}DTO::class,
                'delete_by_criteria' => static function (array $daoCollection): array
                {
                    $deleteByCriteria = [];

                    foreach ($daoCollection as $index => $dao) {
                            $deleteByCriteria[$index]['{prefix}_id'] = (int) $dao->getId();

                    }

                    return $deleteByCriteria;
                },
            ],
        ];
    }
}
