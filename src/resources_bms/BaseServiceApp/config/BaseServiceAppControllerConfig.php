<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\{BaseService}App\config;

use Paneric\Interfaces\Config\ConfigInterface;

class {BaseService}AppControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => '{prefix}'
        ];
    }
}
