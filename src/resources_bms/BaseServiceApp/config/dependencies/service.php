<?php

declare(strict_types=1);

use Paneric\Validation\ValidationService;
use Paneric\Validation\Validator;
use Paneric\Validation\ValidatorBuilder;
use Psr\Container\ContainerInterface;

return [
    Validator::class => static function (ContainerInterface $container): Validator {
        $validatorBuilder = new ValidatorBuilder();

        return $validatorBuilder->build(
            (string) $container->get('local')
        );
    },

    ValidationService::class => static function (ContainerInterface $container): ValidationService {
        $validation = (array) $container->get('validation');

        if ($container->has('seo-validation')) {
            $validation = array_merge($validation, (array) $container->get('seo-validation'));
        }

        return new ValidationService(
            $container->get(Validator::class),
            $validation
        );
    },
];
