<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\{BaseService}Apc\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleApcController;

class {BaseService}ApcController extends BaseModuleApcController {}
