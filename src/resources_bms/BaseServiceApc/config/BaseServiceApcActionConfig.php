<?php

declare(strict_types=1);

namespace {Psr4}\{BaseService}\{BaseService}Apc\config;

use Paneric\Interfaces\Config\ConfigInterface;
use {Psr4}\{BaseService}\Gateway\{BaseService}DAO;

class {BaseService}ApcActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        $apiEndpoints = [
            'base_url' => $_ENV['BASE_API_URL'],

            'api-prefix.get'       => '/api-{prefix}/get/',
            'api-prefixs'          => '/api-{prefix}s/get',
            'api-prefixs.get.page' => '/api-{prefix}s/get/',

            'api-prefix.create'    => '/api-{prefix}/create',
            'api-prefixs.create'   => '/api-{prefix}s/create',

            'api-prefix.update'    => '/api-{prefix}/update/',
            'api-prefixs.update'   => '/api-{prefix}s/update',

            'api-prefix.delete'    => '/api-{prefix}/delete/',
            'api-prefixs.delete'   => '/api-{prefix}s/delete',

            'apc-prefixs.show-all-paginated' => '/apc-{prefix}s/show-all-paginated/',
            'apc-prefixs.edit'               => '/apc-{prefix}s/edit/',
            'apc-prefixs.remove'             => '/apc-{prefix}s/remove/',
        ];

        return [
            'get_one_by_id' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                    'prefix' => '{prefix}'
                ]
            ),

            'get_all' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                    'prefix' => '{prefix}'
                ]
            ),

            'get_all_paginated' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                    'prefix' => '{prefix}'
                ]
            ),


            'create' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                    'prefix' => '{prefix}'
                ]
            ),

            'create_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                    'dao_class' => {BaseService}DAO::class,
                    'prefix' => '{prefix}'
                ]
            ),

            'update' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                    'prefix' => '{prefix}'
                ]
            ),

            'update_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                ]
            ),

            'delete' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                ]
            ),

            'delete_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => '{base_service}',
                ]
            ),
        ];
    }
}
